"""Test the herder module."""
import os
import time
import unittest
from unittest import mock
from urllib.parse import urlparse
import uuid

from cki_lib import misc
from cki_lib import session
import gitlab
import yaml

from pipeline_herder import main
from pipeline_herder import matchers
from pipeline_herder import settings
from pipeline_herder import utils

SESSION = session.get_session(__name__)


def cleanup_gitlab(deletable_objects):
    """Remove merge requests, branches and projects from a GitLab instance."""
    for deletable_object in deletable_objects:
        with misc.only_log_exceptions():
            deletable_object.delete()


@mock.patch.multiple(settings,
                     HERDER_RETRY_LIMIT=3,
                     HERDER_ACTION='retry',
                     HERDER_RETRY_DELAYS=(0,))
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestHerder(unittest.TestCase):
    """Test the herder module."""

    def __init__(self, *args, **kwargs):
        """Create a new test case."""
        super().__init__(*args, **kwargs)
        self.projects = []
        self.gl_project = None
        self.gl_project_personal = None

    @classmethod
    def setUpClass(cls):
        """Set up the Gitlab instances."""
        cls.instance_url = os.environ['IT_GITLAB_URL']
        cls.instance_host = urlparse(cls.instance_url).hostname
        cls.parent_project = os.environ['IT_GITLAB_PARENT_PROJECT']
        cls.token_personal = os.environ['IT_GITLAB_PERSONAL_TOKEN']
        cls.token_bot = os.environ['IT_GITLAB_CI_BOT_TOKEN']
        cls.token_herder = os.environ['IT_GITLAB_HERDER_CI_BOT_TOKEN']
        cls.gl_personal = gitlab.Gitlab(
            cls.instance_url, cls.token_personal,
            session=SESSION)
        cls.gl_personal.auth()
        cls.gl_bot = gitlab.Gitlab(
            cls.instance_url, cls.token_bot,
            session=SESSION)
        cls.gl_bot.auth()
        cls.gl_herder = gitlab.Gitlab(
            cls.instance_url, cls.token_herder,
            session=SESSION)
        cls.gl_herder.auth()
        cls.name_herder = cls.gl_herder.user.username

    def tearDown(self):
        """Clean generated artifacts from the Gitlab instance."""
        cleanup_gitlab(self.projects)

    def _pipeline_jobs(self, gl_pipeline, *, job_name=None):
        """Return all jobs of the pipeline.

        Returns PipelineJob instances.

        Works around https://gitlab.com/gitlab-org/gitlab/-/issues/206908.
        """
        gl_jobs = []
        for gl_job in self.gl_project.jobs.list(iterator=True):
            if (gl_job.pipeline['id'] == gl_pipeline.id and
                    (not job_name or gl_job.name == job_name)):
                gl_jobs.append(gl_job)
            if gl_job.created_at < gl_pipeline.created_at:
                break
        return gl_jobs

    def _wait_job(self, gl_job, until_finished=True):  # pylint: disable=no-self-use
        print(f'Waiting for job to {"finish" if until_finished else "start"}: {gl_job.web_url}')
        for _ in range(120):
            if until_finished:
                if gl_job.finished_at:
                    print(f'Job status: {gl_job.status}')
                    return
            else:
                if gl_job.status == 'running':
                    return
            time.sleep(1)
            gl_job.refresh()
        raise Exception('Job did not finish')

    @mock.patch('pipeline_herder.main.submit_retry')
    def _match_job(self, gl_job, submit_retry=None):
        with mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value=self.token_herder)):
            main.process_job(self.instance_host,
                             self.gl_project.path_with_namespace,
                             gl_job.id)
        return submit_retry

    def _last_job(self, gl_job):
        """Return the last job with the same name."""
        gl_pipeline = self.gl_project.pipelines.get(gl_job.pipeline['id'])
        gl_jobs = self._pipeline_jobs(gl_pipeline, job_name=gl_job.name)
        last_job_id = sorted(gl_jobs, reverse=True, key=lambda j: j.created_at)[0].id
        return self.gl_project.jobs.get(last_job_id)

    def _retry_job(self, gl_job, should_fail=False, other_user=0):
        """Retry a job with the herder, and do some basic sanity checks."""
        print('Retrying job')
        with mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value=self.token_herder)):
            gl_pipeline = self.gl_project.pipelines.get(gl_job.pipeline['id'])
            main.retry(self.instance_host,
                       self.gl_project.path_with_namespace,
                       gl_job.id)
            gl_jobs_new = [j for j in gl_pipeline.jobs.list(iterator=True)
                           if j.name == gl_job.name and j.id > gl_job.id]

            if should_fail:
                self.assertEqual(len(gl_jobs_new), other_user)
                self.assertTrue(all(j.user['username'] != self.name_herder
                                    for j in gl_jobs_new))
                return None

            job_urls = [j.web_url for j in gl_jobs_new]
            print(f'Started jobs: {", ".join(job_urls)}')
            gl_job_last = self._last_job(gl_job)
            self.assertEqual(len(gl_jobs_new), 1)
            self.assertEqual(gl_jobs_new[0].name, gl_job.name)
            self.assertEqual(gl_jobs_new[0].user['username'], self.name_herder)
            self.assertEqual(gl_jobs_new[0].id, gl_job_last.id)
            return gl_job_last

    def _gitlab_ci_yaml(self, retry=False, has_failure=True):
        # pylint: disable=no-self-use
        job_template = {
            'image': 'quay.io/cki/python',
            'only': ['api'],
        }
        if retry:
            job_template['retry'] = 2
        return yaml.safe_dump({
            'stages': ['stage1', 'stage2'],
            '.job': job_template,
            'job1': {
                'extends': '.job', 'stage': 'stage1',
                'script': ['exit 137' if has_failure else 'exit 0'],
            },
            'job2': {
                'extends': '.job', 'stage': 'stage2',
                'script': ['exit 0'],
            }
        }, sort_keys=False)

    def _setup_project(self, gitlab_ci_yml):
        path = str(uuid.uuid4())
        namespace_id = self.gl_bot.namespaces.get(self.parent_project).id
        self.gl_project = self.gl_bot.projects.create({
            'path': path,
            'namespace_id': namespace_id,
            'initialize_with_readme': True})
        print(f'Created project {self.gl_project.name}')
        self.gl_project_personal = self.gl_personal.projects.get(
            self.gl_project.id)
        self.projects.append(self.gl_project_personal)
        payload = {
            'file_path': '.gitlab-ci.yml',
            'branch': 'main',
            'content': gitlab_ci_yml,
            'commit_message': 'GitLab CI configuration'}
        self.gl_project.files.create(payload)

    def _trigger_pipeline(self, variables=None):
        """Trigger and return a pipeline."""
        payload = {
            'ref': 'main',
            'variables': [{'key': key, 'value': value}
                          for key, value in (variables or {}).items()]
        }
        return self.gl_project.pipelines.create(payload)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_matcher(self):
        """Check that a job can be matched."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        submit_retry = self._match_job(gl_job)
        submit_retry.assert_called_once_with(mock.ANY, matchers.MATCHERS[0], 0)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_retry(self):
        """Check that a job can be retried."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_retry_limit(self):
        """Check that a job will be retried with a limit."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        # only 3 retries + original job
        self._retry_job(gl_job, should_fail=True)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_retry_retriggered(self):
        """Check that a job will not be retried if it was retriggered."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline({'CKI_DEPLOYMENT_ENVIRONMENT': 'retrigger'})
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        self._retry_job(gl_job, should_fail=True)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_retry_action(self):
        """Check that a job will not be retried for HERDER_ACTION=report."""

        # needs to be a context manager as already present on class
        with mock.patch.object(settings, 'HERDER_ACTION', 'report'):
            self._setup_project(self._gitlab_ci_yaml())
            gl_pipeline = self._trigger_pipeline()
            gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
            self._wait_job(gl_job)

            self._retry_job(gl_job, should_fail=True)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_retry_gitlab(self):
        """Check that the herder is able to cope with GitLab retries."""

        self._setup_project(self._gitlab_ci_yaml(retry=True))
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        # will be retried by GitLab
        self._retry_job(gl_job, should_fail=True, other_user=1)
        gl_job = self._last_job(gl_job)
        self._wait_job(gl_job)

        # will be retried by GitLab
        self._retry_job(gl_job, should_fail=True, other_user=1)
        gl_job = self._last_job(gl_job)
        self._wait_job(gl_job)

        # now we can retry one more time
        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        # the 2 retries by GitLab are taken into account
        self._retry_job(gl_job, should_fail=True)

    @mock.patch('pipeline_herder.main.notify_finished')
    def test_message_sent(self, notify_finished):
        """Check that a message is sent if the jobs finishes correctly."""
        self._setup_project(self._gitlab_ci_yaml(has_failure=False))
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        self._match_job(gl_job)
        notify_finished.assert_called_with(
            self.instance_host,
            self.gl_project.path_with_namespace,
            gl_job.id
        )

    @mock.patch('pipeline_herder.main.notify_finished')
    def test_message_not_sent(self, notify_finished):
        """Check that a message is not sent if the jobs fails."""
        self._setup_project(self._gitlab_ci_yaml(has_failure=True))
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job1')[0]
        self._wait_job(gl_job)

        self._match_job(gl_job)
        self.assertFalse(notify_finished.called)

    # disabled as it currently fails because an initial failed 'build' job is needed
    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def _test_depending_jobs(self):
        """Check that DependingJobsMatcher works as expected."""
        ci_yaml = yaml.safe_dump({
            'stages': ['stage1', 'stage2'],
            '.job': {
                'image': 'quay.io/cki/python',
                'only': ['api'],
                'script': ['exit 0'],
            },
            'build': {'extends': '.job', 'stage': 'stage1'},
            'publish': {'extends': '.job', 'stage': 'stage2'},
        }, sort_keys=False)

        self._setup_project(ci_yaml)
        gl_pipeline = self._trigger_pipeline()
        job_build = self._pipeline_jobs(gl_pipeline, job_name='build')[0]
        job_publish = self._pipeline_jobs(gl_pipeline, job_name='publish')[0]
        self._wait_job(job_publish)

        matchers.MATCHERS = [
            utils.DependingJobsMatcher(
                job_name='build',
                depending_job_name='publish',
            )
        ]

        submit_retry = self._match_job(job_build)
        submit_retry.assert_called_once_with(mock.ANY, matchers.MATCHERS[0], 0)

    @mock.patch('pipeline_herder.main.notify_finished', mock.Mock())
    def test_retry_running(self):
        """Check that a running job can be retried."""
        ci_yaml = yaml.safe_dump({
            'stages': ['stage1'],
            'job': {
                'image': 'quay.io/cki/python',
                'only': ['api'],
                'script': ['sleep 120'],
                'stage': 'stage1'
            },
        }, sort_keys=False)

        self._setup_project(ci_yaml)
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_jobs(gl_pipeline, job_name='job')[0]
        self._wait_job(gl_job, until_finished=False)

        # Wait a few seconds just in case
        time.sleep(5)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job, until_finished=False)
        self.assertEqual(2, len(self._pipeline_jobs(gl_pipeline, job_name='job')))
